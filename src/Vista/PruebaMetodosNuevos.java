/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Interface.IOperacion2;
import Negocio.Matriz_Aleatorio;
import Negocio.Tripleta;
import Negocio.Vector_Aleatorio;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author julia
 */
public class PruebaMetodosNuevos {

    public static void main(String[] args) {
        Vector_AleatorioPrueba();
        Matriz_AleatorioPrueba();
        TripletaPrueba();
    }

    public static void Vector_AleatorioPrueba() {
        try {
            System.out.println("Digite el tamaño del arreglo");
            Scanner lector = new Scanner(System.in);
            int tamanio = lector.nextInt();

            //:D
            Vector_Aleatorio myVector = new Vector_Aleatorio(tamanio);
            System.out.println(myVector.toString());
            //FORMA 1: Creando los objetos de la clase e invocando sus metodos.
            myVector.interCambio();
            System.out.println("FORMA 1:\n" + myVector.toString());

            //FORMA 2: Polimorfismo.
            IOperacion2 interface2 = myVector;
            interface2.interCambio();
            System.out.println("FORMA 2:\n" + interface2.toString());

            //FORMA 3: Casting de la interface.
            myVector.interCambio();
            String vector = ((IOperacion2) myVector).toString();
            System.out.println("FORMA 3:\n" + vector);

        } catch (Exception ex) {
            System.err.println("Error:" + ex.getMessage());
        }
    }

    public static void Matriz_AleatorioPrueba() {
        try {
            System.out.println("Por favor digite datos de la matriz...");
            Scanner lector = new Scanner(System.in);
            System.out.println("Por favor digite cantidad de filas:");
            int filas = lector.nextInt();
            System.out.println("Por favor digite cantidad de columnas:");
            int cols = lector.nextInt();
            System.out.println("Por favor digite limiInicial de elementos aleatorios:");
            int ini = lector.nextInt();
            System.out.println("Por favor digite limifinal de elementos aleatorios:");
            int fin = lector.nextInt();

            // :D
            Matriz_Aleatorio myMatriz = new Matriz_Aleatorio(filas, cols);
            myMatriz.crearElementos_Aleatorios(ini, fin);
            System.out.println("\n"+myMatriz.toString());

            //FORMA 1: Creando los objetos de la clase e invocando sus metodos.
            myMatriz.interCambio();
            System.out.println("FORMA 1:\n" + myMatriz.toString());

            //FORMA 2: Polimorfismo.
            IOperacion2 interface2 = myMatriz;
            interface2.interCambio();
            System.out.println("FORMA 2:\n" + interface2.toString());

            //FORMA 3: Casting de la interface.
            myMatriz.interCambio();
            String matriz = ((IOperacion2) myMatriz).toString();
            System.out.println("FORMA 3:\n" + matriz);

        } catch (Exception ex) {
            System.err.println("Error:" + ex.getMessage());
        }
    }

    public static void TripletaPrueba() {
        Tripleta tripleta = new Tripleta(3, 6, 9);
        System.out.println(tripleta.toString());
        //FORMA 1: Creando los objetos de la clase e invocando sus metodos.
        tripleta.interCambio();
        System.out.println("FORMA 1:\n" + tripleta.toString());

        //FORMA 2: Polimorfismo.
        IOperacion2 interface2 = tripleta;
        interface2.interCambio();
        System.out.println("Forma 2:\n" + interface2.toString());

        //FORMA 3: Casting de la interface.
        tripleta.interCambio();
        String tripleta1 = ((IOperacion2) tripleta).toString();
        System.out.println("FORMA 3:\n" + tripleta1);
    }

}
