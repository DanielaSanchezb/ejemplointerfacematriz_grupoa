/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Interface.IOperacion;
import Interface.IOperacion2;

/**
 *
 * @author madar
 */
public class Vector_Aleatorio implements IOperacion, IOperacion2{
    
    int vector[];

    public Vector_Aleatorio(int n) throws Exception {
        if(n<=0)
            throw new Exception("No se puede crear el vector:");

        this.vector=new int[n];
        this.crearNumeros(n);
    }
    
    private void crearNumeros(int n)
    {
        
     for(int i=0;i<n;i++)   
     {
         this.vector[i]=i;
     }
    
    }

    public int[] getVector() {
        return vector;
    }

    public void setVector(int[] vector) {
        this.vector = vector;
    }

    @Override
    public String toString() {
        
       if(this.vector==null)
            return ("Vector vacío");
        String msg="";
        for(int dato:this.vector)
            msg+=dato+"\t";
        
        return msg;

        
    }

    @Override
    public int getTotal() {
        int t=0;
        for(int dato:this.vector)
           t+=dato;
        
        return t;

    }

    @Override
    public void interCambio() {
        int aux = 0;
        for (int i=0; i<vector.length/2; i++) {
            aux = vector[i];
            vector[i] = vector[vector.length-1-i];
            vector[vector.length-1-i] = aux;
        }
    }

    
    
    
}
